package com.dekhoapp.android.app.baselib.internal;

/**
 * @author Aidan Follestad (afollestad)
 */
interface CameraUriInterface {

    String getOutputUri();
}
