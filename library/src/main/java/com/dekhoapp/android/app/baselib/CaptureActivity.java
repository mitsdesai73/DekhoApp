package com.dekhoapp.android.app.baselib;

import android.app.Fragment;
import android.support.annotation.NonNull;

import com.dekhoapp.android.app.baselib.internal.BaseCaptureActivity;
import com.dekhoapp.android.app.baselib.internal.CameraFragment;

public class CaptureActivity extends BaseCaptureActivity {

    @Override
    @NonNull
    public Fragment getFragment() {
        return CameraFragment.newInstance();
    }
}