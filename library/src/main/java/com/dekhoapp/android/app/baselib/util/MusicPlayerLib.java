package com.dekhoapp.android.app.baselib.util;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.dekhoapp.android.app.baselib.MaterialCamera;

/**
 * Created by mits on 6/6/17.
 */

public class MusicPlayerLib extends Service {

    Context context;
   Uri uri= MaterialCamera.my_uri;
    public MusicPlayerLib() {
    }

    public MusicPlayerLib(Context context) {
        this.context = context;
    }

    MediaPlayer mp;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        mp=MediaPlayer.create(context,uri);
        mp.setLooping(false);

    }

    @Override
    public void onDestroy() {
        mp.stop();
    }

    public void start(){
        mp.start();
    }

}
