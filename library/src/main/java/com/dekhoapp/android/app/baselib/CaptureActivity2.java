package com.dekhoapp.android.app.baselib;

import android.app.Fragment;
import android.support.annotation.NonNull;

import com.dekhoapp.android.app.baselib.internal.BaseCaptureActivity;
import com.dekhoapp.android.app.baselib.internal.Camera2Fragment;

public class CaptureActivity2 extends BaseCaptureActivity {

    @Override
    @NonNull
    public Fragment getFragment() {
        return Camera2Fragment.newInstance();
    }
}