package com.dekhoapp.android.app.base;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class AddMusic extends AppCompatActivity {

    FloatingActionButton fab_hindi,fab_english,fab_melody,fab_open;
    LinearLayout lin_hindi,lin_english,lin_melody;
    String song_category;
    ProgressDialog progress_dialog;
    private StorageReference storage_reference;
    AlertDialog.Builder alertDialogBuilder;


    Animation show_button;
    Animation Hide_button;
    Animation show_layout;
    Animation Hide_layout;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_music);

        alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Exit").setMessage(("Are you sure?"));

        progress_dialog=new ProgressDialog(this);
        storage_reference= FirebaseUtil.getStorageReference();

        fab_hindi=(FloatingActionButton)findViewById(R.id.activity_add_music_fab_hindi);
        fab_english=(FloatingActionButton)findViewById(R.id.activity_add_music_fab_english);
        fab_melody=(FloatingActionButton)findViewById(R.id.activity_add_music_fab_melody);
        fab_open=(FloatingActionButton)findViewById(R.id.activity_add_music_fab_open);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("All Music"));
        tabLayout.addTab(tabLayout.newTab().setText("Category Music"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        final ViewPager view_Pager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        view_Pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        fab_hindi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ChooseFile("Hindi");

            }
        });

        fab_english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseFile("English");

            }
        });

        fab_melody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseFile("Melody");
            }
        });

        lin_hindi=(LinearLayout) findViewById(R.id.activity_add_music_lin_hindi);
        lin_english=(LinearLayout)findViewById(R.id.activity_add_music_lin_english);
        lin_melody=(LinearLayout)findViewById(R.id.activity_add_music_lin_melody);

          show_button= AnimationUtils.loadAnimation(AddMusic.this,R.anim.show_floating_button);
         Hide_button= AnimationUtils.loadAnimation(AddMusic.this,R.anim.hide_floating_button);
        show_layout= AnimationUtils.loadAnimation(AddMusic.this,R.anim.show_layout);
            Hide_layout= AnimationUtils.loadAnimation(AddMusic.this,R.anim.hide_layout);


        fab_open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(lin_hindi.getVisibility()==View.VISIBLE){

                    fab_open.startAnimation(Hide_button);
                    lin_hindi.startAnimation(Hide_layout);
                    lin_english.startAnimation(Hide_layout);
                    lin_melody.startAnimation(Hide_layout);

                    lin_hindi.setVisibility(View.INVISIBLE);
                    lin_english.setVisibility(View.INVISIBLE);
                    lin_melody.setVisibility(View.INVISIBLE);


                }
                else {


                    lin_hindi.setVisibility(View.VISIBLE);
                    lin_english.setVisibility(View.VISIBLE);
                    lin_melody.setVisibility(View.VISIBLE);

                    fab_open.startAnimation(show_button);
                    lin_hindi.startAnimation(show_layout);
                    lin_english.startAnimation(show_layout);
                    lin_melody.startAnimation(show_layout);

                }
            }
        });



    }

    private void ChooseFile(String song_cat) {
        song_category=new String (song_cat);
        Intent intent_upload = new Intent();
        intent_upload.setType("audio/*");
        intent_upload.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent_upload,1);
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        progress_dialog.setTitle("Uploading..!!");
        if(requestCode==1){
            if(resultCode==RESULT_OK){
                Uri uri=data.getData();
                String filename=getFileName(uri);
                Toast.makeText(this, ""+filename, Toast.LENGTH_SHORT).show();

                progress_dialog.show();

                if(uri!=null){

                    UploadFile(uri,filename);

                }

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void UploadFile(Uri uri,String file) {
        StorageReference ref=storage_reference.child("Mitul/Music/"+song_category+"/"+file);
        ref.putFile(uri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        progress_dialog.dismiss();
                        Toast.makeText(AddMusic.this, "Music Uploaded Successfully..!!", Toast.LENGTH_SHORT).show();
                        fab_open.startAnimation(Hide_button);
                        lin_hindi.startAnimation(Hide_layout);
                        lin_english.startAnimation(Hide_layout);
                        lin_melody.startAnimation(Hide_layout);

                        lin_hindi.setVisibility(View.INVISIBLE);
                        lin_english.setVisibility(View.INVISIBLE);
                        lin_melody.setVisibility(View.INVISIBLE);

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        progress_dialog.dismiss();
                        Toast.makeText(AddMusic.this, "Uploading Failed..!!", Toast.LENGTH_SHORT).show();
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                double progress=(100.0*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                progress_dialog.setMessage(((int)progress)+"% Uploaded..!!");

            }
        });


    }

    @Override
    public void onBackPressed() {

        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
    @Override
     public void onClick(DialogInterface dialog, int which) {

    }
}).create().show();

    }
}
