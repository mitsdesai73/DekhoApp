package com.dekhoapp.android.app.base;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by mits on 6/6/17.
 */

public class MusicPlayer extends Service {

    Context context;
    Uri uri=AllMusic_Fragment.my_uri;

    public MusicPlayer() {
    }

    public MusicPlayer(Context context) {
        this.context = context;
    }

    MediaPlayer mp;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        mp=MediaPlayer.create(context,uri);
        mp.setLooping(false);

    }

    @Override
    public void onDestroy() {
        mp.stop();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        mp.start();

    }
}
