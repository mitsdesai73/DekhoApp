package com.dekhoapp.android.app.base;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

/**
 * Created by mits on 31/5/17.
 */

public class AllMusic_Fragment extends Fragment {

    RecyclerView recyclerView;
    All_Music_Adapter adapter;
    ArrayList<Music_Model> musicModel;
    public static Uri my_uri;

    FirebaseDatabase database;
    DatabaseReference myRef;
    StorageReference storageRef;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        musicModel=new ArrayList<Music_Model>();
        storageRef=FirebaseUtil.getStorageReference();
        database= FirebaseDatabase.getInstance();
        myRef= database.getReference("Mitul");
        View view=inflater.inflate(R.layout.all_music_fragment, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.All_music_recyclerView);
        adapter=new All_Music_Adapter(getContext(),musicModel);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                musicModel.clear();
                for(DataSnapshot datasnap:dataSnapshot.getChildren()){
                    for(DataSnapshot data:datasnap.getChildren()){
                        String name= (String) data.child("title").getValue();
                        String url= (String) data.child("url").getValue();

                        Music_Model music=new Music_Model(name,url);
                        musicModel.add(music);

                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                     //   Toast.makeText(getContext(), ""+musicModel.get(position).getUrl(), Toast.LENGTH_SHORT).show();


                        storageRef.child(musicModel.get(position).getUrl()).getDownloadUrl()
                                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                    //    Toast.makeText(getContext(), ""+uri.toString(), Toast.LENGTH_SHORT).show();
                                        my_uri=uri;

                                        startActivity(new Intent(getContext(),AccessCameraActivity.class));

                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(getContext(), "Failed..!!", Toast.LENGTH_SHORT).show();

                                    }
                                });


                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );



        return view;

    }



}
