package com.dekhoapp.android.app.base;

/**
 * Created by mits on 5/6/17.
 */

public class Music_Model {

    String title;
    String url;

    public Music_Model(String title, String url) {
        this.title = title;
        this.url = url;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
