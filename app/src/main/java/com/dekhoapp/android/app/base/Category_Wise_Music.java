package com.dekhoapp.android.app.base;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class Category_Wise_Music extends AppCompatActivity {

    RecyclerView recyclerView;
    All_Music_Adapter adapter;
    ArrayList<Music_Model> musicModel;

    FirebaseDatabase database;
    DatabaseReference myRef;
    StorageReference storageRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category__wise__music);

        Bundle extra=getIntent().getExtras();

        String category=extra.getString("Category");


        musicModel=new ArrayList<Music_Model>();
        storageRef=FirebaseUtil.getStorageReference();
        database= FirebaseDatabase.getInstance();
        myRef= database.getReference("Mitul/"+category);
        recyclerView=(RecyclerView)findViewById(R.id.Category_music_recyclerView);
        adapter=new All_Music_Adapter(getApplicationContext(),musicModel);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                musicModel.clear();
                for(DataSnapshot datasnap:dataSnapshot.getChildren()){

                    String name= (String) datasnap.child("title").getValue();
                    String url= (String) datasnap.child("url").getValue();

                    Music_Model music=new Music_Model(name,url);
                    musicModel.add(music);


                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                    //    Toast.makeText(getApplicationContext(), ""+musicModel.get(position).getTitle(), Toast.LENGTH_SHORT).show();


                        storageRef.child(musicModel.get(position).getUrl()).getDownloadUrl()
                                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                      //  Toast.makeText(getApplicationContext(), ""+uri.toString(), Toast.LENGTH_SHORT).show();
                                       AllMusic_Fragment.my_uri=uri;

                                        startActivity(new Intent(getApplicationContext(),AccessCameraActivity.class));

                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(getApplicationContext(), "Failed..!!", Toast.LENGTH_SHORT).show();

                                    }
                                });

                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

    }
}
