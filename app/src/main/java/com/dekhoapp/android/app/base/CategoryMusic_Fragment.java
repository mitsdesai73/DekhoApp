package com.dekhoapp.android.app.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by mits on 31/5/17.
 */

public class CategoryMusic_Fragment extends Fragment {

    private CardView card_hindi,card_english,card_melody;
    private FrameLayout framelayout;
    static  String Song_Category;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.category_music_fragment, container, false);

        card_hindi=(CardView)view.findViewById(R.id.card_view_hindi);
        card_english=(CardView)view.findViewById(R.id.card_view_english);
        card_melody=(CardView)view.findViewById(R.id.card_view_melody);
        framelayout=(FrameLayout)view.findViewById(R.id.framelayout);


        card_hindi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(getContext(), "Hindi Music", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getActivity(),Category_Wise_Music.class);
                intent.putExtra("Category","Hindi");
                startActivity(intent);
            }
        });

        card_english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(getContext(), "English Music", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getActivity(),Category_Wise_Music.class);
                intent.putExtra("Category","English");
                startActivity(intent);
            }
        });

        card_melody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(getContext(), "Melody Music", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getActivity(),Category_Wise_Music.class);
                intent.putExtra("Category","Melody");
                startActivity(intent);
            }
        });

        return view;

    }
}
