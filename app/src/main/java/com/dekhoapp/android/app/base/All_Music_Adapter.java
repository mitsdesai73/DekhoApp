package com.dekhoapp.android.app.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by mits on 5/6/17.
 */

public class All_Music_Adapter extends RecyclerView.Adapter<All_Music_Adapter.ViewHolder> {
    private Context context;
    private ArrayList<Music_Model> musicModel;


    public All_Music_Adapter(Context context, ArrayList<Music_Model> musicModel) {
        this.context = context;
        this.musicModel = musicModel;
    }

    @Override
    public All_Music_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.single_music,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(All_Music_Adapter.ViewHolder holder, int position) {

        holder.txt_name.setText(musicModel.get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        return musicModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name;
        public ViewHolder(View itemView) {

            super(itemView);
            txt_name=(TextView)itemView.findViewById(R.id.all_music_txt_name);
        }
    }
}
