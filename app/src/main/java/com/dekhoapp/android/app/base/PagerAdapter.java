package com.dekhoapp.android.app.base;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by mits on 31/5/17.
 */

public class PagerAdapter extends FragmentStatePagerAdapter{
    int NoOfTabs=0;
    public PagerAdapter(FragmentManager fm,int NoOfTabs) {
        super(fm);
        this.NoOfTabs=NoOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                AllMusic_Fragment all_music=new AllMusic_Fragment();
                return all_music;

            case 1:
                CategoryMusic_Fragment categoryMusicFragment=new CategoryMusic_Fragment();
                return categoryMusicFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NoOfTabs;
    }
}
